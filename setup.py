from setuptools import setup, find_packages

setup(
    name='mimic_etl',
    version='0.0.1',
    description='Configurable ETL for MIMIC-III',
    url='https://github.com/n-s-f/mimic-etl',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=['taskmap', 'pandas', 'psycopg2', 'aiopg'],
    package_data={'mimic_etl': ['queries/*.sql', 'queries/**/*.sql']},
)
