from mimic_etl.db import py_view, sql_view, read, build_selection_query
from psycopg2 import sql
from paco import partial

import pandas as pd
import psycopg2
import taskmap
import typing
import os


class ETLTask(typing.NamedTuple):
    name: str
    query: str
    func: typing.Callable[[pd.DataFrame], pd.DataFrame] = None
    deps: typing.List[str] = list()


class format_dict(dict):
    def __missing__(self, key):
        return '{' + key + '}'


def build_event_query(event, transforms=tuple()):
    event_dict = event._asdict()
    event_dict['itemids'] = ','.join(str(i) for i in event_dict['itemids'])

    query = event.template.format_map(format_dict(event_dict))
    for transform in transforms:
        query = transform.format_map(format_dict(event_query=query))

    return query


def load_query(path):
    src_dir = os.path.dirname(os.path.realpath(__file__))
    query_dir = os.path.join(src_dir, 'queries')
    with open(os.path.join(query_dir, path), 'r') as handle:
        return handle.read()


def load_transforms(transformations):
    return [
        load_query('transformations/{}.sql'.format(transformation))
        for transformation in transformations
    ]


def run(etlconfig, tasks, taskset='all', slow=None):
    writers = {
        '%s_wr' % task.name: build_write_task(task, etlconfig)
        for task in tasks
    }

    readers = {
        task.name: build_read_task(task, etlconfig)
        for task in tasks
    }

    writer_deps = {
        '%s_wr' % task.name: task.deps + (['static_wr'] if task.name != 'static' else [])
        for task in tasks
    }

    reader_deps = {task.name: ['%s_wr' % task.name] for task in tasks}

    if slow is None:
        slow = [
            'sbp', 'dbp', 'temp', 'resp_rate', 'meanbp', 'spo2', 'heart_rate',
            'ventilation', 'vasopressor'
        ]
    slow += ['%s_wr' % task.name for task in tasks]

    create_dest_schema(etlconfig, taskset)

    if taskset == 'write':
        graph = taskmap.create_graph(writers, writer_deps, slow, name='mimic')
        return taskmap.run_parallel_async(graph)

    if taskset == 'read':
        reader_deps = {task.name: [] for task in tasks}
        graph = taskmap.create_graph(readers, reader_deps, slow, name='mimic')

    elif taskset != 'read':
        funcs = {**readers, **writers}
        deps = {**reader_deps, **writer_deps}
        graph = taskmap.create_graph(funcs, deps, slow, name='mimic')

    return taskmap.run_async(graph)


def create_dest_schema(etlconfig, taskset):
    if taskset == 'read':
        return

    with psycopg2.connect(**etlconfig.dbconfig._asdict()) as connection:
        with connection.cursor() as cursor:
            param = sql.Identifier(etlconfig.dest_schema)
            query = sql.SQL('create schema if not exists {};').format(param)
            cursor.execute(query)


def build_write_task(task, etlconfig):
    task_dict = task._asdict()

    if task.func is None:
        view = sql_view
        task_dict['query'] = """
            drop materialized view if exists {dest_schema}.{name} cascade;
            create materialized view {dest_schema}.{name} as
            {query};
        """.format_map(format_dict(query=task.query, name=task.name))

    elif task.func is not None:
        view = py_view
        task_dict['query'] = '{query};'.format_map(format_dict(query=task.query))

    return partial(view, etlconfig, **task_dict)


def build_read_task(task, etlconfig):
    query = build_selection_query(task.name, etlconfig.dest_schema)
    return partial(read, etlconfig, query)
