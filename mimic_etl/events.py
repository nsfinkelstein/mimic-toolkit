from mimic_etl.etl import load_query

import typing


class Event(typing.NamedTuple):
    name: str
    template: str
    itemids: typing.Tuple[int, ...] = tuple()
    lower_limit: float = -1e6
    upper_limit: float = 1e6


events = {
    # chartevents
    'heart_rate': Event(
        name='heart_rate',
        itemids=(211, 220045),
        lower_limit=0,
        upper_limit=300,
        template=load_query('events/chart.sql'),
    ),

    'meanbp': Event(
        name='meanbp',
        itemids=(456, 52, 6702, 443, 220052, 220181, 225312),
        lower_limit=0,
        upper_limit=300,
        template=load_query('events/chart.sql'),
    ),

    'glucose': Event(
        name='glucose',
        itemids=(807, 811, 1529, 3745, 3744, 225664, 220621, 226537),
        lower_limit=0,
        template=load_query('events/chart.sql'),
    ),

    'spo2': Event(
        name='spo2',
        itemids=(646, 220277),
        lower_limit=0,
        upper_limit=100,
        template=load_query('events/chart.sql'),
    ),

    'resp_rate': Event(
        name='resp_rate',
        itemids=(615, 618, 220210, 224690),
        lower_limit=0,
        upper_limit=70,
        template=load_query('events/chart.sql'),
    ),

    'sbp': Event(
        name='sbp',
        itemids=(6, 51, 455, 6701, 220050, 220179),
        lower_limit=0,
        upper_limit=400,
        template=load_query('events/chart.sql'),
    ),

    'dbp': Event(
        name='dbp',
        itemids=(8368, 8440, 8441, 8555, 220180, 220051),
        lower_limit=0,
        upper_limit=300,
        template=load_query('events/chart.sql'),
    ),

    'temperature': Event(
        name='temperature',
        template=load_query('events/temperature.sql'),
    ),

    'fio2': Event(
        name='fio2',
        itemids=(3420, 190, 223835, 3422),
        lower_limit=21,
        upper_limit=100,
        template=load_query('events/fio2.sql'),
    ),


    # labevents
    'bun': Event(
        name='bun',
        itemids=(51006, ),
        lower_limit=0,
        upper_limit=300,
        template=load_query('events/lab.sql'),
    ),

    'creatinine': Event(
        name='creatinine',
        itemids=(50912, ),
        lower_limit=0,
        upper_limit=150,
        template=load_query('events/lab.sql'),
    ),

    'glucose_lab': Event(
        name='glucose_lab',
        itemids=(50809, 50931),
        lower_limit=0,
        upper_limit=10000,
        template=load_query('events/lab.sql'),
    ),

    'bicarbonate': Event(
        name='bicarbonate',
        itemids=(50882, ),
        lower_limit=0,
        upper_limit=10000,
        template=load_query('events/lab.sql'),
    ),

    'hematocrit': Event(
        name='hematocrit',
        itemids=(50810, 51221),
        lower_limit=0,
        upper_limit=100,
        template=load_query('events/lab.sql'),
    ),

    'lactate': Event(
        name='lactate',
        itemids=(50813, ),
        lower_limit=0,
        upper_limit=50,
        template=load_query('events/lab.sql'),
    ),

    'platelet': Event(
        name='platelet',
        itemids=(51265, ),
        lower_limit=0,
        upper_limit=10000,
        template=load_query('events/lab.sql'),
    ),

    'potassium': Event(
        name='potassium',
        itemids=(50822, 50971),
        lower_limit=0,
        upper_limit=30,
        template=load_query('events/lab.sql'),
    ),

    'sodium': Event(
        name='sodium',
        itemids=(50824, 50983),
        lower_limit=0,
        upper_limit=200,
        template=load_query('events/lab.sql'),
    ),

    'wbc': Event(
        name='wbc',
        itemids=(51300, 51301),
        lower_limit=0,
        upper_limit=1000,
        template=load_query('events/lab.sql'),
    ),

    'magnesium': Event(
        name='magnesium',
        itemids=(50960,),
        lower_limit=0,
        upper_limit=100,
        template=load_query('events/lab.sql'),
    ),

    'albumin': Event(
        name='albumin',
        itemids=(50862, ),
        lower_limit=0,
        upper_limit=10,
        template=load_query('events/lab.sql'),
    ),

    'anion_gap': Event(
        name='anion_gap',
        itemids=(50868, ),
        lower_limit=0,
        upper_limit=10000,
        template=load_query('events/lab.sql'),
    ),

    'bilirubin': Event(
        name='bilirubin',
        itemids=(50885, ),
        lower_limit=0,
        upper_limit=150,
        template=load_query('events/lab.sql'),
    ),

    'chloride': Event(
        name='chloride',
        itemids=(50806, 50902),
        lower_limit=0,
        upper_limit=10000,
        template=load_query('events/lab.sql'),
    ),

    'hemoglobin': Event(
        name='hemoglobin',
        itemids=(50811, 51222),
        lower_limit=0,
        upper_limit=50,
        template=load_query('events/lab.sql'),
    ),

    'ptt': Event(
        name='ptt',
        itemids=(51275, ),
        lower_limit=0,
        upper_limit=150,
        template=load_query('events/lab.sql'),
    ),

    'inr': Event(
        name='inr',
        itemids=(51237, ),
        lower_limit=0,
        upper_limit=50,
        template=load_query('events/lab.sql'),
    ),

    'pt': Event(
        name='pt',
        itemids=(51274, ),
        lower_limit=0,
        upper_limit=150,
        template=load_query('events/lab.sql'),
    ),

    # outputevents
    'urine': Event(
        name='urine',
        template=load_query('events/urine.sql'),
    ),

    # ioevents
    'rbc_transfusion': Event(
        name='rbc_transfusion',
        itemids=(30179, 42324, 42588, 30001, 30004, 42239, 225168),
        template=load_query('events/transfusion.sql'),
    ),

    'ffp_transfusion': Event(
        name='ffp_transfusion',
        itemids=(30005, 30180, 44044, 220970),
        template=load_query('events/transfusion.sql'),
    ),

    'norepinephrine': Event(
        name='norepinephrine',
        itemids=(30047, 30120, 221906),
        template=load_query('events/transfusion.sql'),

    ),

    'fentanyl': Event(
        name='fentanyl',
        itemids=(30150, 30308, 30118, 30149, 43387, 221744, 225972, 225942),
        template=load_query('events/transfusion.sql'),

    ),

    # durations
    'ventilation_duration': Event(
        name='ventilation_duration',
        template=load_query('durations/ventilation.sql'),
    ),

    'vasopressor_duration': Event(
        name='vasopressor_duration',
        template=load_query('durations/vasopressor.sql'),
    ),
}
