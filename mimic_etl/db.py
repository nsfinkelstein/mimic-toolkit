from psycopg2 import sql

import pandas as pd
import aiopg


def prep_query(query, etlconfig):
    params = {
        'source_schema': sql.Identifier(etlconfig.source_schema),
        'dest_schema': sql.Identifier(etlconfig.dest_schema),
    }

    schema_change = sql.SQL('set search_path to {source_schema};')

    if type(query) is str:
        query = sql.SQL(query)

    if type(query) is not sql.Composed:
        query = query.format(**params)

    return schema_change.format(**params) + query


async def py_view(etlconfig, query, name, func):
    df = await read(etlconfig, query)
    result = func(df)
    await write(etlconfig, name, result)


async def sql_view(etlconfig, query, **args):
    async with aiopg.connect(**etlconfig.dbconfig._asdict()) as connection:
        async with connection.cursor() as cursor:
            query = prep_query(query, etlconfig)
            await cursor.execute(query, timeout=etlconfig.timeout)


async def read(etlconfig, query):
    async with aiopg.connect(**etlconfig.dbconfig._asdict()) as connection:
        async with connection.cursor() as cursor:
            query = prep_query(query, etlconfig)
            await cursor.execute(query, timeout=etlconfig.timeout)
            records = await cursor.fetchall()
            df = pd.DataFrame.from_records(records)
            df.columns = [d[0].lower() for d in cursor.description]
            return df


async def write(etlconfig, name, df):
    async with aiopg.connect(**etlconfig.dbconfig._asdict()) as connection:
        async with connection.cursor() as cursor:
            creation = get_creation_query(name, df, etlconfig)
            insertion = get_insertion_query(name, df, etlconfig)
            query = creation + insertion
            params = df.values.flatten().tolist()
            query = prep_query(query, etlconfig)
            await cursor.execute(query, params, timeout=etlconfig.timeout)


def get_creation_query(name, df, etlconfig):
    deletion = 'drop table if exists {dest_schema}.{name} cascade;'

    cols = ('{} {}'.format(col, get_col_type(col, df)) for col in df.columns)
    col_list = ','.join(cols)
    creation = 'create table {dest_schema}.{name} (%s);' % col_list

    return sql.SQL(deletion + creation).format(
        name=sql.Identifier(name),
        dest_schema=sql.Identifier(etlconfig.dest_schema),
    )


def get_insertion_query(name, df, etlconfig):
    cols = (sql.Identifier(col) for col in df.columns)
    cols_insertion = sql.SQL(', ').join(cols)

    record = (sql.Placeholder() for _ in range(len(df.columns)))
    record_insertion = sql.SQL(', ').join(record)

    all_records = (record_insertion for _ in range(len(df)))
    all_records_insertion = sql.SQL('), (').join(all_records)

    insertion = 'insert into {dest_schema}.{name} ({cols}) values ({records});'
    return sql.SQL(insertion).format(
        name=sql.Identifier(name),
        cols=cols_insertion,
        records=all_records_insertion,
        dest_schema=sql.Identifier(etlconfig.dest_schema),
    )


def get_col_type(col, df):
    type_str = str(df[col].dtype).lower()

    if 'int' in type_str:
        return 'int'

    if 'float' in type_str:
        return 'float'

    if 'timedelta' in type_str:
        return 'interval'

    if 'datetime' in type_str:
        return 'timestamp without time zone'

    return 'text'


def build_selection_query(name, dest_schema, limit=None):
    if limit is not None and type(limit) != int:
        raise ValueError('Limit must be an integer')

    query = 'select * from {dest_schema}.{name}'
    close = 'limit {limit};'.format(limit) if limit else ';'
    return sql.SQL(query + close).format(
        dest_schema=sql.Identifier(dest_schema),
        name=sql.Identifier(name),
    )
