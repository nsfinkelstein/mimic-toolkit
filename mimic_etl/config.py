import typing


class DBConfig(typing.NamedTuple):
    user: str = 'postgres'
    password: str = 'postgres'
    port: str = '5432'
    host: str = '127.0.0.1'
    dbname: str = 'mimic'


class ETLConfig(typing.NamedTuple):
    dbconfig: DBConfig = DBConfig()
    source_schema: str = 'mimiciii'
    dest_schema: str = 'mimiciii'
    timeout: int = 3600  # 1 hr
