# Durations

Queries in this directory should select relations with at least the following 
columns:

- icustay_id
- starttsp (time after icustay intime that the duration began)
- endtsp (time after icustay intime that the duration concluded)

Events should only include icustay_ids present in the the `static` view.
