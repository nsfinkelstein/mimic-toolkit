-- adapted from:
-- https://github.com/MIT-LCP/mimic-code/blob/9e05ab27b947b664f7ddbc5dcfe0c3e573c69fcc/concepts/durations/ventilation-durations.sql

with mechanical as (
  select icustay_id, charttime - intime as tsp, 'mechanical'::text as event
  from chartevents
  join static
    using (icustay_id)
  where value is not null and (
    (itemid = 720 and value != 'Other/Remarks')                 -- VentTypeRecorded
    or (itemid = 223848 and value != 'Other')
    or (itemid = 467 and value = 'Ventilator')                  -- O2 delivery device == ventilator
    or itemid in (445, 448, 449, 450, 1340, 1486, 1600, 224687  -- minute volume
      , 639, 654, 681, 682, 683, 684,224685,224684,224686       -- tidal volume
      , 218,436,535,444,459,224697,224695,224696,224746,224747  -- High/Low/Peak/Mean/Neg insp force ("RespPressure")
      , 221,1,1211,1655,2000,226873,224738,224419,224750,227187 -- Insp pressure
      , 543                                                     -- PlateauPressure
      , 5865,5866,224707,224709,224705,224706                   -- APRV pressure
      , 60,437,505,506,686,220339,224700                        -- PEEP
      , 3459                                                    -- high pressure relief
      , 501,502,503,224702                                      -- PCV
      , 223,667,668,669,670,671,672                             -- TCPCV
      , 224701                                                  -- PSVlevel
      , 223849                                                  -- ventilator mode
    )
  )
),

extubation as (
  select icustay_id, charttime - intime as tsp, 'extubation'::text as event
  from chartevents
  join static
    using (icustay_id)
  where value is not null and (
    (itemid = 640 and value = 'Extubated')
    or (itemid = 640 and value = 'Self Extubation')
  )

  union

  -- add in the extubation flags from procedureevents_mv
  select icustay_id, starttime - intime as tsp, 'extubation'::text as event
  from procedureevents_mv
  join static
    using (icustay_id)
  where itemid in (
    227194   -- "Extubation"
    , 225468 -- "Unplanned Extubation (patient-initiated)"
    , 225477 -- "Unplanned Extubation (non-patient initiated)"
  )
),

oxygen as (
  select icustay_id, charttime - intime as tsp, 'oxygen'::text as event
  from chartevents
  join static
    using (icustay_id)
  where (itemid = 226732 and value in (
      'Nasal cannula',           -- 153714 observations
      'Face tent',               -- 24601 observations
      'Aerosol-cool',            -- 24560 observations
      'Trach mask ',             -- 16435 observations
      'High flow neb',           -- 10785 observations
      'Non-rebreather',          -- 5182 observations
      'Venti mask ',             -- 1947 observations
      'Medium conc mask ',       -- 1888 observations
      'T-piece',                 -- 1135 observations
      'High flow nasal cannula', -- 925 observations
      'Ultrasonic neb',          -- 9 observations
      'Vapomist'                 -- 3 observations
    ))
    or (itemid = 467 and value in (
      'Cannula',                 -- 278252 observations
      'Nasal Cannula',           -- 248299 observations
      'None',                    -- 95498 observations
      'Face Tent',               -- 35766 observations
      'Aerosol-Cool',            -- 33919 observations
      'Trach Mask',              -- 32655 observations
      'Hi Flow Neb',             -- 14070 observations
      'Non-Rebreather',          -- 10856 observations
      'Venti Mask',              -- 4279 observations
      'Medium Conc Mask',        -- 2114 observations
      'Vapotherm',               -- 1655 observations
      'T-Piece',                 -- 779 observations
      'Hood',                    -- 670 observations
      'Hut',                     -- 150 observations
      'TranstrachealCat',        -- 78 observations
      'Heated Neb',              -- 37 observations
      'Ultrasonic Neb'           -- 2 observations
    ))
),

settings_lag as (
  select icustay_id
    , tsp
    , event
    , tsp - lag(tsp) over (partition by icustay_id, event order by tsp) as delta
  from (
    select * from mechanical union
    select * from extubation union
    select * from oxygen
  ) as allevents
  order by icustay_id, tsp
),

boundaries as (
  select icustay_id
    , tsp
    , delta
    , case
        when (lag(event) over (partition by icustay_id order by tsp) != event
              or (delta > '8 hours'::interval and event = 'mechanical')
              or (delta > '24 hours'::interval and event = 'oxygen')
              or delta is null)
            and event in ('oxygen', 'mechanical')
          then event
        else null
      end as new_event
  from settings_lag
),

vent_counts as (
  select icustay_id
    , tsp
    , new_event
    , sum(case when new_event is null then 0 else 1 end) over
        (partition by icustay_id order by tsp) as ventnum
  from boundaries
)

select icustay_id
  , min(new_event) as venttype  -- only recorded at new_event time
  , min(tsp) as starttsp
  , max(tsp) as endtsp
  , ventnum
from vent_counts
where ventnum > 0
group by icustay_id, ventnum
having max(tsp) != min(tsp)
order by icustay_id, min(tsp)
