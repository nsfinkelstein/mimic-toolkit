# Views

Queries in this directory should be templated for an {event_query} that select a
relation with the following columns:

- icustay_id
- valuenum (the relevent value (e.g. valuenum from chartevents or text from 
  noteevents))
- valueuom (the units of measurement (e.g. valueuom from chartevents or category
  from noteevents))
- tsp (time since icustay intime that the event transpired)

The transformation should then select a relation with those same four columns.
These transformations are composable as long as the column types remain the
same.

Please see queries in this directory for examples.
