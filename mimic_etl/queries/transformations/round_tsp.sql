-- Rounds observation to the nearest hour. If more than one observation is
-- recorded for an hour, they are averaged.

select avg(valuenum) as valuenum
  , valueuom
  , icustay_id
  , date_trunc('hour', tsp + '30 min'::interval) as tsp  -- round to hour
from ({event_query}) as events
group by valueuom, icustay_id, date_trunc('hour', tsp + '30 min'::interval)

