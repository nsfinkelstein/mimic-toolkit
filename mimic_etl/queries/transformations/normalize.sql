-- Calculates the zscore for the valuenum column

select (valuenum - avg(valuenum) over ()) / stddev(valuenum) over() as valuenum
  , valueuom
  , icustay_id
  , tsp
from ({event_query}) as events
