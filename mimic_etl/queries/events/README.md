# Events

Queries in this directory should select relations with at least the following
columns:

- icustay_id
- valuenum (the relevent value (e.g. valuenum from chartevents or text from 
  noteevents))
- valueuom (the units of measurement (e.g. valueuom from chartevents or category
  from noteevents))
- tsp (time since icustay intime that the event transpired)

Events should only include icustay_ids present in the the `static` view.
