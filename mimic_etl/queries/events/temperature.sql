select case
    when itemid in (223761, 678) then (valuenum - 32) / 1.8  -- convert to °C
    else valuenum
  end as valuenum
  , ev.valueuom
  , icustay_id
  , ev.charttime - st.intime as tsp
from chartevents as ev
inner join {dest_schema}.static as st
  using (icustay_id)
where (valuenum > 10 and valuenum < 50 and itemid in (223762, 676))
  or (valuenum > 70 and valuenum < 120 and itemid in (223761, 678))
  and error is distinct from 1
