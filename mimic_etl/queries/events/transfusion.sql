with raw as (
  select io.amount
    , io.amountuom
    , io.icustay_id
    , io.charttime - st.intime as tsp
  from inputevents_cv as io
  inner join {dest_schema}.static as st
    using (icustay_id)
  where itemid in ({itemids})
    and amount > 0

  union

  select io.amount
    , io.amountuom
    , io.icustay_id
    , io.starttime - st.intime as tsp
  from inputevents_mv as io
  inner join {dest_schema}.static as st
    using (icustay_id)
  where itemid in ({itemids})
    and amount > 0
),

cumulative as (
  select sum(amount) over (partition by icustay_id order by tsp desc) as amount
    , amountuom
    , icustay_id
    , tsp
    , tsp - lag(tsp) over (partition by icustay_id order by tsp) as delta
  from raw
)

-- We consider any transfusions started within 1 hr of the last one
-- to be part of the same event
select amount - case
      when row_number() over (partition by icustay_id order by tsp desc) = 1 then 0
      else lag(amount) over (partition by icustay_id order by tsp desc) 
    end as amount
  , amountuom
  , icustay_id
  , tsp
from cumulative
where delta is null or delta > '1 hour'::interval
order by icustay_id, tsp
