select ev.valuenum
  , ev.valueuom
  , st.icustay_id
  , ev.charttime - st.intime as tsp
from chartevents as ev
inner join {dest_schema}.static as st
  using (icustay_id)
where itemid in ({itemids})
  and valuenum > {lower_limit}
  and valuenum < {upper_limit}
  and error is distinct from 1
