select case
    when itemid = 227488 and value > 0 then -1 * value
    else value
  end as valuenum
  , ev.valueuom
  , icustay_id
  , ev.charttime - st.intime as tsp
from outputevents as ev
inner join {dest_schema}.static as st
  using (icustay_id)
where itemid in (
  -- these are the most frequently occurring urine output observations in carevue
  40055, -- "urine out foley"
  43175, -- "urine ."
  40069, -- "urine out void"
  40094, -- "urine out condom cath"
  40715, -- "urine out suprapubic"
  40473, -- "urine out ileoconduit"
  40085, -- "urine out incontinent"
  40057, -- "urine out rt nephrostomy"
  40056, -- "urine out lt nephrostomy"
  40405, -- "urine out other"
  40428, -- "urine out straight cath"
  40086,--	urine out incontinent
  40096, -- "urine out ureteral stent #1"
  40651, -- "urine out ureteral stent #2"

  -- these are the most frequently occurring urine output observations in metavision
  226559, -- "foley"
  226560, -- "void"
  226561, -- "condom cath"
  226584, -- "ileoconduit"
  226563, -- "suprapubic"
  226564, -- "r nephrostomy"
  226565, -- "l nephrostomy"
  226567, --	straight cath
  226557, -- r ureteral stent
  226558, -- l ureteral stent
  227488, -- gu irrigant volume in
  227489  -- gu irrigant/urine volume out
)
