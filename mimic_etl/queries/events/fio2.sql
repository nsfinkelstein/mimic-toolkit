with fio2 as (
  select max(
    case
      when itemid = 223835 then
        case
          when valuenum > 0 and valuenum <= 1 then valuenum * 100
          when valuenum > 1 and valuenum < 21 then null
          when valuenum >= 21 and valuenum <= 100 then valuenum
          else null
        end
      when itemid in (3420, 3422) then valuenum 
      when itemid = 190 and valuenum > 0.20 and valuenum < 1 then valuenum * 100
      else null
    end) as valuenum
    , ev.valueuom
    , icustay_id
    , ev.charttime - st.intime as tsp
  from chartevents as ev
  inner join {dest_schema}.static as st
    using (icustay_id)
  where itemid in (3420, 190, 223835, 3422)
    and error is distinct from 1
  group by st.subject_id, st.hadm_id, st.intime, icustay_id, ev.charttime, ev.valueuom
)

select *
from fio2
where valuenum is not null
