select ev.valuenum
  , ev.valueuom
  , st.icustay_id
  , ev.charttime - st.intime as tsp
from labevents as ev
inner join {dest_schema}.static as st
  using (hadm_id)
where charttime - intime > '0 days'::interval
  and charttime - intime < stay_duration
  and itemid in ({itemids})
  and valuenum > {lower_limit}
  and valuenum < {upper_limit}
