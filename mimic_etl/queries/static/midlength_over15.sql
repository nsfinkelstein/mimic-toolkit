with height as (
  select icustay_id
    , avg(
        case
          when itemid in (920, 1394, 4187, 3486) then valuenum * 0.0254  -- in
          else valuenum * 0.01  -- cm
        end
      ) as valuenum
  from chartevents
  where (itemid in (920, 1394, 4187, 3486) and valuenum > 24 and valuenum < 96)
    or (itemid in (226730, 3485, 4188) and valuenum > 50  and valuenum < 250)
  group by icustay_id
),

weight as (
  select icustay_id, avg(valuenum) as valuenum
  from (
    select icustay_id, valuenum, charttime
    from chartevents
    where itemid in (762, 226512, 763, 224639) -- admit / daily weight in kg
      and valuenum > 20 and valuenum < 300  -- bounds
  ) as ce
  inner join icustays as icu
    using (icustay_id)
  where ce.charttime <= icu.intime + '6 hours'::interval
    and ce.charttime >= icu.intime - '6 hours'::interval
  group by icustay_id
)

select ad.hadm_id
  , ad.subject_id
  , icu.icustay_id
  , icu.intime
  , icu.outtime - icu.intime as stay_duration
  , ad.ethnicity
  , ad.diagnosis
  , ad.admission_type
  , pt.gender
  , date_part('year', age(icu.intime, pt.dob)) as age
  , icu.first_careunit as icu
  , height.valuenum as height
  , weight.valuenum as weight
  , weight.valuenum / (height.valuenum ^ 2) as bmi
from patients as pt
inner join admissions as ad
  using (subject_id)
inner join icustays as icu
  using (hadm_id)
left join height
  using (icustay_id)
left join weight
  using (icustay_id)
where icu.outtime - icu.intime < '360 hours'::interval
  and icu.outtime - icu.intime > '6 hours'::interval
  and date_part('year', age(icu.intime, pt.dob)) > 15
  and date_part('year', age(icu.intime, pt.dob)) < 120
order by subject_id, intime
