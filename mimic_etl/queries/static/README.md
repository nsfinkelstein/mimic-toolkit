# Static

Queries in this directory should specify a cohort for the ETL. They should
select relations with at least the following columns, as well as any further
static features of interest.

- subject_id
- hadm_id
- icustay_id
- icustay_intime
