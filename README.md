# mimic-etl

This library is designed to facilitate reproducible, accurate and quick ETL 
pipelines from the [MIMIC-III](https://mimic.physionet.org/about/mimic/) 
database.

**The library is currently under active development; use at your own risk.**

## Quickstart

Here is a small working example that demonstrates a typical `mimic-etl` workflow:

```.py
from mimic_etl import events, etl, config

etlconfig = config.ETLConfig(
    dbconfig=config.DBConfig(
        user='mimic3',
        password='mimic',
    ),
    source_schema='public',
    dest_schema='demo',
)

# create task to define cohort
static_query = etl.load_query('static/midlength_over15.sql')
tasks = [etl.ETLTask(name='static', query=static_query)]

# create tasks to define dynamic features
dynamic_features = [
    # vitals
    'heart_rate',
    'meanbp',

    # labs
    'bun',
    'creatinine',
    
    # input / output
    'urine',
]

tasks += [
    etl.ETLTask(
        name=name,
        query=etl.build_event_query(
            event=events.events[name],
            transforms=etl.load_transforms(('normalize', 'round_tsp'))
        ),
    ) for name in dynamic_features]

# create tasks to define outcomes
outcomes = [
    'ventilation_duration',
    'vasopressor_duration',
]

tasks += [
        etl.ETLTask(
        name=name,
        query=etl.build_event_query(
            event=events.events[name]
        )
    ) for name in outcomes]

# write views to database
etl.run(etlconfig, tasks, taskset='write');

# read views from database
data = etl.run(etlconfig, tasks, taskset='read').results

# all views are now available in the data dictionary
print(data['meanbp'].head())
#    valuenum  valueuom  icustay_id      tsp
# 0 -0.108735      mmHg      200001 00:00:00
# 1 -0.168120      mmHg      200001 01:00:00
# 2  0.010034      mmHg      200001 02:00:00
# 3  0.010034      mmHg      200001 03:00:00
# 4 -0.465042      mmHg      200001 04:00:00
```

The library also allows for using custom event and transform templates, and for
declaring dependencies that views may have on each other. For more detail on
this additional functionality please see the 
[demo](https://github.com/n-s-f/mimic-etl/blob/master/examples/etl_demo.ipynb)
notebook.

## Installation

The recommend method of installation is:

```.bash
git clone git@github.com:n-s-f/mimic-etl.git
cd mimic-etl
pip install --editable ./
```

Changes made to the python and sql code in the `mimic-etl` directory will then
be picked up.

## Library structure

This library is built around several core concepts: cohorts, events, and
transformations. Each has corresponding built in templates, but it is always
possible to pass in a custom query that is not defined in this library. 

If you do construct a custom query for use with this library, please consider
making a pull request.

### Cohort

Each ETL pipeline begins by specifying a cohort. This is done by creating a
static view with the desired selection criteria, as well as all static
information that does not change over the course of an icu stay. As an example, 
the template used in the script above is 
[here](mimic_etl/queries/static/midlength_over15.sql).

All built-in templates for creating cohorts will be found in the 
[queries/static](mimic_etl/queries/static) directory.

### Event

Event queries should return valid records for a particular event type, e.g.
heart rate. Often this will entail identifying the relevant itemids and making 
sure the observed value is within some range. For simple views of this nature,
the default [chart](mimic_etl/queries/events/chart.sql) or 
[lab](mimic_etl/queries/events/lab.sql) templates can be used.

There are also events that require additional preprocessing. In this case,
custom templates can be used. Some of these are built in. For example
the [temperature](mimic_etl/queries/events/temperature.sql) template converts
all readings to celsius.

Any dynamic information - information that changes with time - should be
conceived of as an event.

### Transforms

The static and dynamic information for your cohort gets saved in to views in the
database. We want the event templates to be entirely reusable between
applications, so none of the transformation takes place there.

Instead, we define transformation templates. The event query gets templated into
the transformation templates. For example, the 
[normalize](mimic_etl/queries/transformations/normalize.sql) template calculates
the zscore of the `valuenum` column.

It is possible to apply more than one transformation, as demonstrated in the
quickstart script above, so long as the transformations keep the column types
consistent.

## Project Goals

The aim is for this library to become a tool for creating reproducible data
extractions from the MIMC-III database, as well as a center for developing
shared familiarity with the database through community contributed event objects
and cohort queries, event and transformation templates.
